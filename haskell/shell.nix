{ nixpkgs ? import (fetchTarball "https://nixos.org/channels/nixos-18.03-small/nixexprs.tar.xz") { } }:
let
  inherit (nixpkgs) pkgs;
  gnunet = (nixpkgs.pkgs.callPackage ../gnunet/gnunet-dev.nix { }).dev;
  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
          bindings-DSL c2hs c2hsc gnunet hspec hspec-discover text stm network-transport cereal distributed-process
        ]);
in
pkgs.stdenv.mkDerivation {
  name = "gnunet-hs-env";
  buildInputs = [ ghc gnunet ];
  shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
}
