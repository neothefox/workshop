## Exchanging HELLOs

A HELLO message is a peer id and associated transport addresses; you can exchange them manually in lieu of other discovery methods.

```sh
gnunet-peerinfo -g > our-hello
gnunet-peerinfo -p < their-hello
```

## Getting info

```sh
gnunet-arm -I # see running services
gnunet-peerinfo --self # our peer id
gnunet-peerinfo --info # peers we know
gnunet-transport --monitor # monitor transport connections
gnunet-core --monitor # and core ones
gnunet-cadet --peers # cadet tunnels and paths to known peers
```
