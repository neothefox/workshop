# Materials for a GNUnet workshop

Find development (three nodes connected with unix sockets) and production (no WAN traffic limit) config in `dev` and `production` directories; see `CHEATSHEET.md` for some oft-used CLI commands.

## Plan

1. Intro talk
2. Get GNUnet and launch local nodes
3. Discuss architecture and services: transport, core, cadet, fs, conversations, pt, scalarproduct
4. Poke at demos: yo
5. gnunet-c / bindings-gnunet / gnunet-hs / network-transport-cadet / ch-cadet-pingpong

## Installing

### GNUnet

Building GNUnet with Nix and make:

```sh
git clone https://gitlab.org/wldhx-gnunet/gnunet.git
cp nix/default.nix gnunet/
cd gnunet
nix-shell
# ...and then proceed as per README.md
# (bootstrap, configure --prefix=$PWD/dist/, make, make install)
```

### C

https://gitlab.com/wldhx-gnunet/cadet-listen

### Haskell

```
cd haskell
git clone https://gitlab.com/wldhx-gnunet/bindings-gnunet

nix-shell --run "export | grep NIX | grep 'gnunet-dev/lib'"
# copy smth that looks like /nix/store/4p43nxazsn9ml7r4v991zmr58whbddg4-gnunet-dev/lib
# and add it to bindings-gnunet/bindings-gnunet.cabal below the `extra-libraries` stanza
# so that it looks like so:
#                        ...
#                      , gnunetcadet
#  extra-lib-dirs:      /nix/store/4p43nxazsn9ml7r4v991zmr58whbddg4-gnunet-dev/lib

git clone https://gitlab.com/wldhx-gnunet/gnunet-hs
git clone https://gitlab.com/wldhx-gnunet/network-transport-cadet
git clone https://gitlab.com/wldhx-gnunet/ch-cadet-pingpong
nix-shell
cabal new-repl gnunet
cabal new-run network-transport-cadet
```

## Docs

https://grothoff.org/christian/habil.pdf: system overview
gnunet/doc/documentation: C tutorial, concepts, developer docs
https://gnunet.org/git/gnunet-java.git/plain/doc/gnunet-java-tutorial.pdf
